/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brainwinner.conexred.distapp.cupos.formatter;

import com.brainwinner.conexred.distapp.cupos.util.LogFormatter;
import com.brainwinner.conexred.distapp.cupos.util.ValidarCampos;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_CONN_DB_97;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_GENERAL_99;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_PARAM_INVALID_96;
import com.brainwinner.connection.BWGConnection;
import com.brainwinner.util.IFormatter;
import com.brainwinner.util.LogSocketListener;
import com.brainwinner.util.ReadPropertiesFile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

/**
 *
 * @author Hernan.Rodriguez
 */
public class PasarCupoFormatter implements IFormatter {


    BWGConnection aCon = null;
    private static LogSocketListener log = null;
    private static final String LOG_NAME = "DISTRIBUIDOR_APP_PASAR_CUPO";
    ReadPropertiesFile readProperties;
    Connection sqlcon = null;

    public PasarCupoFormatter(BWGConnection bCon) {
        this.aCon = bCon;
        log = LogFormatter.getLog(log, LOG_NAME);
        this.readProperties = ReadPropertiesFile.getInstance();
    }

    @Override
    public ISOMsg erouted(ISOMsg isomsg) {
        log.log("****************************Inicio Pasar Cupo****************************");

        /*Iso de respusta*/
        ISOMsg response = null;
        
        /*Array con campos a validar*/
        ArrayList<Integer> fields = new ArrayList<Integer>();
        fields.add(4);
        fields.add(32);
        fields.add(42);
        fields.add(46);
        

        /*Objetos para la base de datos*/
        PreparedStatement ps = null;
        ResultSet rs = null;

            try {
                
                if(!ValidarCampos.validISOFields(isomsg, fields, log)){
                    response.set(new ISOField(39, ERR_PARAM_INVALID_96 ));
                    response.set(new ISOField(63, "ERR_PARAM_INVALID_96"));
                    return response;
                }
                
                response = (ISOMsg) isomsg.clone();
                response.setResponseMTI();
                this.sqlcon = this.aCon.getConnection();
                if (this.sqlcon != null) {
                    log.log("Datos al procedimiento: ");
                    log.log("1: " + isomsg.getString(4).trim()); //monto
                    log.log("2: " + Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(32).trim()))); //idcomercio
                    log.log("3: " + isomsg.getString(42).trim()); //cedula
                    log.log("4: " + "secret password" );
//                    log.log("3: " + Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(32).trim())));

                    ps = this.sqlcon.prepareStatement("select * from \"APPDISTRIBUIDOR\".procesarcupoprepago(?,?,?,?)");
                    ps.setString(1, ISOUtil.zeroUnPad(isomsg.getString(42).trim()));
                    ps.setString(2, ISOUtil.zeroUnPad(isomsg.getString(46).trim()));
                    ps.setInt(3, Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(32).trim())));
                    ps.setInt(4, Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(4).trim())));
                             
                    log.log("Ejecutando Procedimiento almacenado...");
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        log.log("SP Response code: " + rs.getInt("idresponse"));
                        log.log("Message: " + rs.getString("response"));
                        Integer idresponse = rs.getInt("idresponse");
                            switch (idresponse) {
                                case 1:
                                    response.set(new ISOField(39, "00"));
                                    response.set(new ISOField(63, rs.getString("response"))); //Login Exitoso
                                    //Campos pendientes
                                    break;
                                default:
                                    response.set(new ISOField(39, idresponse.toString()));
                                    response.set(new ISOField(63, rs.getString("response"))); // Cuenta Bloqueada
                                    break;
                            }
                    } else {
                        log.log("Sin respuesta del procedimiento");
                        response.set(new ISOField(39, ERR_GENERAL_99));
                        response.set(new ISOField(63, "Error general"));
                    }
                } else {
                    log.log("Sin conexión a la base de datos");
                    response.set(new ISOField(39, ERR_CONN_DB_97));
                    response.set(new ISOField(63, "Error de conexión con la base de datos"));
                }
            } catch (Exception e) {
            log.log("Excecpxion " +e.getClass().getSimpleName()+ " en el formatter");
            log.log("Exc " + e.getMessage());
            e.printStackTrace();
            try {
                response.set(new ISOField(39, ERR_GENERAL_99));
                response.set(new ISOField(63, "Error general"));
            } catch (Exception ex) {
                log.log(ex.getMessage());
            }
        }finally {
                try {
                    if (this.sqlcon != null) this.sqlcon.close();
                    if (rs != null) rs.close();
                    if (ps != null) ps.close();
                } catch (SQLException ex) {
                    log.log(ex.getMessage());
                }
            }
        return response;
    }
}
