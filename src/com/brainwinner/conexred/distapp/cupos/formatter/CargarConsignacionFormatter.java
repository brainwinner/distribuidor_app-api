package com.brainwinner.conexred.distapp.cupos.formatter;

import com.brainwinner.conexred.distapp.cupos.util.LogFormatter;
import com.brainwinner.conexred.distapp.cupos.util.ValidarCampos;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_CONN_DB_97;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_GENERAL_99;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_PARAM_INVALID_96;
import com.brainwinner.connection.BWGConnection;
import com.brainwinner.util.IFormatter;
import com.brainwinner.util.LogSocketListener;
import com.brainwinner.util.ReadPropertiesFile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

public class CargarConsignacionFormatter
        implements IFormatter {

    BWGConnection aCon = null;
    private static LogSocketListener log = null;
    private static final String LOG_NAME = "DISTRIBUIDOR_APP_CARGAR_CONSIGNACION";
    ReadPropertiesFile readProperties;
    Connection sqlcon = null;

    public CargarConsignacionFormatter(BWGConnection bCon) {
        this.aCon = bCon;
        log = LogFormatter.getLog(log, LOG_NAME);
        this.readProperties = ReadPropertiesFile.getInstance();
    }

    @Override
    public ISOMsg erouted(ISOMsg isomsg) {
        log.log("****************************Inicio Cargar Consignacion****************************");

        /*Iso de respusta*/
        ISOMsg response = (ISOMsg) isomsg.clone();

        /*Array con campos a validar*/
        ArrayList<Integer> fields = new ArrayList<Integer>();
        fields.add(4);
        fields.add(37);
        fields.add(42);
        fields.add(44);
        fields.add(46);
        fields.add(82);
        fields.add(83);
        fields.add(104);
        fields.add(114);
        fields.add(124);
        fields.add(125);
        
        /*Objetos para la base de datos*/
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            
            if(!ValidarCampos.validISOFields(isomsg, fields, log)){
                response.set(new ISOField(39, ERR_PARAM_INVALID_96 ));
                response.set(new ISOField(63, "ERR_PARAM_INVALID_96"));
                return response;
            }
            
            response.setResponseMTI();
            this.sqlcon = this.aCon.getConnection();
            if (this.sqlcon != null) {
                log.log("Datos al procedimiento: ");
                log.log("Pos 1: " + ISOUtil.zeroUnPad(isomsg.getString(42).trim()));
                log.log("Pos 2: " + "Secret password...");
                log.log("Pos 3: " + isomsg.getString(104).trim());
                log.log("Pos 4: " + Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(82).trim())));
                log.log("Pos 5: " + Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(83).trim())));
                log.log("Pos 6: " + isomsg.getString(44).trim());
                log.log("Pos 7: " + isomsg.getString(37).trim());
                log.log("Pos 8: " + isomsg.getString(125).trim());
                log.log("Pos 9: " + Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(4).trim())));
                log.log("Pos 10: " + isomsg.getString(114).trim());
                log.log("Pos 11: " + isomsg.getString(124).trim());
                
                
                ps = this.sqlcon.prepareStatement("select * from \"APPDISTRIBUIDOR\".cargar_consignacion(?,?,?,?,?,?,?,?,?,?,?)");
                ps.setString(1, isomsg.getString(42).trim());//Cuenta
                ps.setString(2, isomsg.getString(46).trim());//Clave
                ps.setString(3, isomsg.getString(104).trim());//Fecha
                ps.setInt(4, Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(82).trim())));//idDistribuidor
                ps.setInt(5, Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(83).trim())));//Banco
                ps.setString(6, isomsg.getString(44).trim());//Departamento
                ps.setString(7, isomsg.getString(37).trim());//Ciudad
                ps.setString(8, isomsg.getString(125).trim());//Sucursal
                ps.setInt(9, Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(4).trim())));//Valor
                ps.setString(10, isomsg.getString(114).trim());//ImagenRuta
                ps.setString(11, isomsg.getString(124).trim());//NumeroCOnsignacion
                
                log.log("Ejecutando Procedimiento almacenado...");
                rs = ps.executeQuery();
                if (rs.next()) {
                    String responseMessage = rs.getString("response").substring(3).toLowerCase();
                    Integer idresponse = rs.getInt("idresponse");
                    log.log("SP Response code: " + rs.getInt("idresponse"));
                    log.log("Message: " + responseMessage);
                    switch (idresponse) {
                        case 1:
                            response.set(new ISOField(39, "00"));
                            response.set(new ISOField(63, responseMessage)); //Login Exitoso
                            break;
                        default:
                            response.set(new ISOField(39, idresponse.toString()));
                            response.set(new ISOField(63, responseMessage)); 
                            break;
                    }
                } else {
                    log.log("Sin respuesta del procedimiento");
                    response.set(new ISOField(39, ERR_GENERAL_99));
                    response.set(new ISOField(63, "Error general"));
                }
            } else {
                log.log("Sin conexión a la base de datos");
                response.set(new ISOField(39, ERR_CONN_DB_97));
                response.set(new ISOField(63, "Sin conexión a la base de datos"));
            }
         } catch (Exception e) {
            log.log("Excecpxion " +e.getClass().getSimpleName()+ " en el formatter");
            log.log("Exc " + e.getMessage());
            e.printStackTrace();
            try {
                response.set(new ISOField(39, ERR_GENERAL_99));
                response.set(new ISOField(63, "Error general"));
            } catch (Exception ex) {
                log.log(ex.getMessage());
            }
        }finally {
            try {
                if (this.sqlcon != null) {
                    this.sqlcon.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                log.log(ex.getMessage());
            }
        }
        log.log(
                "*******************Fin transaccion****************************");
        return response;
    }
}
