package com.brainwinner.conexred.distapp.cupos.formatter;

import com.brainwinner.conexred.distapp.cupos.util.LogFormatter;
import com.brainwinner.conexred.distapp.cupos.util.ValidarCampos;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_CONN_DB_97;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_GENERAL_99;
import static com.brainwinner.conexred.distapp.cupos.util.ValidarCampos.ERR_PARAM_INVALID_96;
import com.brainwinner.connection.BWGConnection;
import com.brainwinner.util.IFormatter;
import com.brainwinner.util.LogSocketListener;
import com.brainwinner.util.ReadPropertiesFile;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

public class EditarMargenProductoFormatter
        implements IFormatter {

    BWGConnection aCon = null;
    private static LogSocketListener log = null;
    private static final String LOG_NAME = "DISTRIBUIDOR_APP_EDITAR_MARGEN_PRODUCTO";
    ReadPropertiesFile readProperties;
    Connection sqlcon = null;

    public EditarMargenProductoFormatter(BWGConnection bCon) {
        this.aCon = bCon;
        log = LogFormatter.getLog(log, LOG_NAME);
        this.readProperties = ReadPropertiesFile.getInstance();
    }

    @Override
    public ISOMsg erouted(ISOMsg isomsg) {
        log.log("****************************Inicio Editar Margen Producto****************************");

        /*Iso de respusta*/
        ISOMsg response = (ISOMsg) isomsg.clone();

        /*Array con campos a validar*/
        ArrayList<Integer> fields = new ArrayList<Integer>();
        fields.add(32);
        fields.add(42);
        fields.add(46);
        fields.add(82);
        fields.add(83);

        /*Objetos para la base de datos*/
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            
            if(!ValidarCampos.validISOFields(isomsg, fields, log)){
                response.set(new ISOField(39, ERR_PARAM_INVALID_96 ));
                response.set(new ISOField(63, "ERR_PARAM_INVALID_96"));
                return response;
            }
            
            response.setResponseMTI();
            this.sqlcon = this.aCon.getConnection();
            if (this.sqlcon != null) {
                /*Datos al log*/
                BigDecimal margen = new BigDecimal(ISOUtil.zeroUnPad(isomsg.getString(83).trim()));
                log.log("Datos al procedimiento:");
                log.log("1: " + isomsg.getString(42).trim());
                log.log("2: Secret Password....");
                log.log("3: " + Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(32).trim())));
                log.log("4: " + Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(82).trim())));
                log.log("5: " + margen);

                /*Preparar y ejecutar el procedimiento*/
                ps = this.sqlcon.prepareStatement("select * from \"APPDISTRIBUIDOR\".editar_margen_producto(?,?,?,?,?)");
                ps.setString(1, isomsg.getString(42).trim());
                ps.setString(2, isomsg.getString(46).trim());
                ps.setInt(3, Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(32).trim())));
                ps.setInt(4, Integer.parseInt(ISOUtil.zeroUnPad(isomsg.getString(82).trim())));
                ps.setBigDecimal(5, margen);

                log.log("Ejecutando Procedimiento almacenado...");
                rs = ps.executeQuery();
                if (rs.next()) {
                    log.log("SP Response code: " + rs.getInt("idresponse"));
                    log.log("Message: " + rs.getString("response"));
                    Integer idresponse = rs.getInt("idresponse");
                    switch (idresponse) {
                        case 1:
                            response.set(new ISOField(39, "00"));
                            response.set(new ISOField(63, rs.getString("response"))); //Login Exitoso
                            break;
                        default:
                            response.set(new ISOField(39, idresponse.toString()));
                            response.set(new ISOField(63, rs.getString("response")));
                            break;
                    }
                } else {
                    log.log("Sin respuesta del procedimiento");
                    response.set(new ISOField(39, ERR_GENERAL_99));
                    response.set(new ISOField(63, "Error general"));
                }
            } else {
                log.log("Sin conexión a la base de datos");
                response.set(new ISOField(39, ERR_CONN_DB_97));
                response.set(new ISOField(63, "Sin conexión a la base de datos"));
            }
         } catch (Exception e) {
            log.log("Excecpxion " +e.getClass().getSimpleName()+ " en el formatter");
            log.log("Exc " + e.getMessage());
            e.printStackTrace();
            try {
                response.set(new ISOField(39, ERR_GENERAL_99));
                response.set(new ISOField(63, "Error general"));
            } catch (Exception ex) {
                log.log(ex.getMessage());
            }
        }finally {
            try {
                if (this.sqlcon != null) this.sqlcon.close();
                if (rs != null) rs.close();
                if (ps != null) ps.close();
            } catch (SQLException ex) {
                log.log(ex.getMessage());
            }
        }
        log.log("*******************Fin Editar Margen Producto****************************");
        return response;
    }
}
