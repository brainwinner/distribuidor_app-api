/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brainwinner.conexred.distapp.cupos.util;

import com.brainwinner.util.LogSocketListener;
import java.util.ArrayList;
import org.jpos.iso.ISOMsg;

/**
 *
 * @author JCruz
 */
public class ValidarCampos {
    
    public static final String ERR_GENERAL_99 = "EG";
    public static final String ERR_CONN_CORE_98 = "98";
    public static final String ERR_CONN_DB_97 = "BX";
    public static final String ERR_PARAM_INVALID_96 = "PI";
    public static final String SUCCESS_1 = "1";
    public static final String CUENTA_INVAL_2 = "2";
    public static final String PASS_INVAL_3 = "3";
    public static final String CUENTA_BLOCK_4 = "4";
    
    public static boolean validISOFields(ISOMsg iso, ArrayList<Integer> fields, LogSocketListener log){
        for(Integer f : fields ){
            if (!iso.hasField(f)){
                log.log("El Formatter espera el campo: "+f);
                return false;
            } 
        }
        return true;
    }
}
