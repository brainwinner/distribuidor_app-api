
package com.brainwinner.conexred.distapp.cupos.util;

import com.brainwinner.util.LogSocketListener;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public  class LogFormatter {
    
    
    public static LogSocketListener getLog  (LogSocketListener log, String logName) {
        if (log == null) {
            PrintStream out = null;
            SimpleDateFormat sdf = null;
            try {
                sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
                out = new PrintStream(new FileOutputStream("/var/local/core/logs/" + logName + "_" + sdf.format(Calendar.getInstance().getTime()) + ".log"));
                log = new LogSocketListener(out);
            } catch (Exception e) {
                log = new LogSocketListener();
                e.printStackTrace();
            }
        }
        return log;
    }
    
    
//    public static void main (String[] arg) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
//        Calendar cal = Calendar.getInstance();
//        System.out.println(sdf.format(Calendar.getInstance().getTime()));
//    } 
    
}
